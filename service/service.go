package service

import (
	"infogetter/storage"
	"infogetter/types/responses"
)

type Service interface {
	GetRecord(id string) (*responses.RecordResponse, error)
}

type service struct {
	storage storage.DataBase
	rdb     storage.RedisStorage
}

func (s *service) GetRecord(id string) (*responses.RecordResponse, error) {
	if val, err := s.rdb.Get(id); err == nil {
		return val, nil
	}
	val, err := s.storage.Get(id)
	if err != nil {
		return nil, err
	}

	if err = s.rdb.Set(id, val); err != nil {
		return nil, err
	}

	return val, nil
}

func NewServer(storage storage.DataBase, rdb storage.RedisStorage) Service {
	return &service{storage: storage, rdb: rdb}
}
