package responses

import "time"

type RecordResponse struct {
	RealID         int       `db:"id" json:"-"`
	ID             string    `db:"uuid" json:"id"`
	Price          float64   `db:"price" json:"price"`
	ExpirationDate time.Time `db:"expiration_date" json:"expiration_date"`
}
