package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"infogetter/docs"
	"infogetter/handler"
	"infogetter/service"
	"infogetter/storage"
)

func main() {
	count := 0
	ticker := time.NewTicker(1 * time.Minute)
	go func() {
		for {
			select {
			case <-ticker.C:
				log.Info().Msg(fmt.Sprintf("ticker started with %d counts", count))
				count = 0
			}
		}
	}()
	if err := godotenv.Load(); err != nil {
		log.Fatal().Msg(err.Error())
	}

	redisDB, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	rdb := storage.NewRedisClient(os.Getenv("REDIS_ADDRESS"), os.Getenv("REDIS_PASSWORD"), redisDB, time.Minute*1)

	psql := storage.NewPostgres(fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME")))

	level, err := zerolog.ParseLevel(os.Getenv("LEVEL"))
	if err != nil {
		log.Err(err).Msg("Failed to parse level, setting to debug level")
		level = zerolog.DebugLevel
	}
	srv := service.NewServer(psql, rdb)
	hndl := handler.NewHandler(srv)
	log.Level(level)

	log.Info().Msg("Starting")

	r := gin.New()
	r.Use(func(c *gin.Context) {
		count++
	})
	//r.Use(gin.Logger())
	r.Use(gin.Recovery())
	docs.SwaggerInfo.Version = "1.0"
	if os.Getenv("RUN_MODE") == "dev" {
		r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		r.GET("/docs/*any", func(c *gin.Context) {
			c.Redirect(http.StatusMovedPermanently, "/swagger/index.html")
		})
	}

	r.GET("/:id", hndl.GetRecord)

	log.Fatal().Msg(r.Run(os.Getenv("ADDRESS")).Error())
}
