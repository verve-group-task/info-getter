package storage

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog/log"

	"infogetter/types/responses"
)

type RedisStorage interface {
	Get(key string) (*responses.RecordResponse, error)
	Set(key string, value *responses.RecordResponse) error
	Delete(key string) error
	Close() error
}

type redisInst struct {
	rdb    *redis.Client
	expire time.Duration
}

func NewRedisClient(address, password string, db int, expire time.Duration) *redisInst {
	client := redis.NewClient(&redis.Options{
		Addr:         address,
		Password:     password,
		DB:           db,
		DialTimeout:  60 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	})

	if _, err := client.Ping(context.TODO()).Result(); err != nil {
		log.Panic().Msg(err.Error())
	}

	return &redisInst{rdb: client, expire: expire}
}

func (r *redisInst) Get(key string) (*responses.RecordResponse, error) {
	var resp = new(responses.RecordResponse)
	res, err := r.rdb.Get(context.Background(), key).Result()
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal([]byte(res), resp); err != nil {
		return nil, err
	}

	return resp, nil
}

func (r *redisInst) Set(key string, value *responses.RecordResponse) error {
	val, err := json.Marshal(value)
	if err != nil {
		return err
	}
	return r.rdb.Set(context.Background(), key, string(val), r.expire).Err()
}

func (r *redisInst) Delete(key string) error {
	return r.rdb.Del(context.Background(), key).Err()
}

func (r *redisInst) Close() error {
	return r.rdb.Close()
}

func (r *redisInst) Flush() error {
	return r.rdb.FlushDB(context.Background()).Err()
}
