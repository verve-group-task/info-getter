package storage

import (
	"infogetter/types/responses"
)

type DataBase interface {
	Get(key string) (*responses.RecordResponse, error)
	Set(id int, value *responses.RecordResponse) error
	Close() error
	Flusher
}

type Flusher interface {
	Flush() error
}
