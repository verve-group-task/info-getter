package storage

import (
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"

	"infogetter/types/responses"
)

type postgres struct {
	db *sqlx.DB
}

const getQuery = "SELECT * FROM promotion WHERE id = $1"

func (p postgres) Get(key string) (*responses.RecordResponse, error) {
	promotion := new(responses.RecordResponse)
	err := p.db.QueryRowx(getQuery, key).StructScan(promotion)
	if err != nil {
		return nil, err
	}

	return promotion, nil
}

const setQuery = "INSERT INTO promotion (id, uuid, price, expiration_date) VALUES ($1, $2, $3, $4) ON CONFLICT (id) DO UPDATE SET uuid = $1, price = $2, expiration_date = $3"

func (p postgres) Set(id int, value *responses.RecordResponse) error {
	_, err := p.db.Exec(setQuery, id, value.ID, value.Price, value.ExpirationDate)
	return err
}

func (p postgres) Close() error {
	return p.db.Close()
}

const flushQuery = "ALTER SEQUENCE promotion_id_seq RESTART WITH 1"

func (p postgres) Flush() error {
	_, err := p.db.Exec(flushQuery)
	return err
}

func NewPostgres(dsn string) *postgres {
	db, err := sqlx.Connect("pgx", dsn)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	return &postgres{db: db}
}
