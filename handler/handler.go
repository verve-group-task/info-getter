package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"

	"infogetter/service"
	"infogetter/types/responses"
)

type Handler interface {
	GetRecord(ctx *gin.Context)
}

type handler struct {
	service service.Service
}

func NewHandler(srv service.Service) Handler {
	return &handler{service: srv}
}

// GetRecord get record
// @Summary get record by id
// @Description get record by id
// @Tags Record
// @Accept json
// @Produce json
// @Param id path int true "get record by id"
// @Success 200 {object} responses.RecordResponse
// @Failure 404 {object} responses.ErrorResponse
// @Router /{id} [get]
func (h *handler) GetRecord(ctx *gin.Context) {
	record, err := h.service.GetRecord(ctx.Param("id"))
	if err != nil {
		log.Err(err).Msg("get record failed")
		ctx.AbortWithStatusJSON(http.StatusNotFound, responses.ErrorResponse{
			Code:    404,
			Message: "not found",
		})
		return
	}

	ctx.JSON(http.StatusOK, record)
}
